/*
 Navicat Premium Data Transfer

 Source Server         : rm-bp121kllv1842l5vwao.mysql.rds.aliyuncs.com
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : rm-bp121kllv1842l5vwao.mysql.rds.aliyuncs.com:3306
 Source Schema         : git_migrate

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 23/06/2021 21:25:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for git_url
-- ----------------------------
DROP TABLE IF EXISTS `git_url`;
CREATE TABLE `git_url`  (
  `id` bigint(64) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1407610558251376661 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
