package life.sunhao.spider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import life.sunhao.spider.entity.GitUrl;

/**
 * @Auther: hsun
 * @Date: 2021/6/23 15:51
 * @Description:
 */
public interface IGitUrlService extends IService<GitUrl> {
}
