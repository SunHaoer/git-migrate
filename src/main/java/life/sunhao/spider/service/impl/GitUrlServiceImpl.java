package life.sunhao.spider.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import life.sunhao.spider.entity.GitUrl;
import life.sunhao.spider.mapper.GitUrlMapper;
import life.sunhao.spider.service.IGitUrlService;
import org.springframework.stereotype.Service;

/**
 * @Auther: hsun
 * @Date: 2021/6/23 15:53
 * @Description:
 */
@Service
public class GitUrlServiceImpl extends ServiceImpl<GitUrlMapper, GitUrl> implements IGitUrlService {
}
