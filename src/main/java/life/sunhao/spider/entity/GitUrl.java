package life.sunhao.spider.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: hsun
 * @Date: 2021/6/23 15:46
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@TableName("git_url")
public class GitUrl extends BaseEntity {

    private String projectName;

    private String url;

}
