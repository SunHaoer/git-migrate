package life.sunhao.spider.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import life.sunhao.spider.common.api.R;
import life.sunhao.spider.entity.GitUrl;
import life.sunhao.spider.service.IGitUrlService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * @Auther: hsun
 * @Date: 2021/6/23 16:07
 * @Description:
 */
@Api(tags = "操作git")
@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/git")
public class GitOptionController {

    final private IGitUrlService gitUrlService;

    // todo 目标git的用户命(git地址前缀)
    final private String targetSourceUrlPrefix = "username/";

    @ApiOperation("创建脚本")
    @GetMapping("/create/cmd")
    public R<Boolean> gitClone() {
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("输出脚本的路径\\git-clone.txt"));
            List<GitUrl> gitUrls = gitUrlService.list();
            for(int i = 0; i < gitUrls.size(); i++) {
                GitUrl gitUrl = gitUrls.get(i);
                // 每次执行停一段时间，以防止太频繁被git拒绝访问
                long sleepTime = (long)(Math.random() * 10) + 5;
                String cmd = "choice /t " + sleepTime + " /d y /n";
                writeCmd(bw, cmd);
                // 根据名称重复
                cmd = "mkdir " + i;
                writeCmd(bw, cmd);
                cmd = "cd " + i;
                writeCmd(bw, cmd);
                // git clone --bare 把整个仓库 clone 成一个 .git 文件
                cmd = "git clone --bare " + gitUrl.getUrl();
                writeCmd(bw, cmd);

                String[] pathArr = gitUrl.getProjectName().split("/");
                // 必须在登录账号名下创建项目
                String fullName = targetSourceUrlPrefix;
                for(int j = 1; j < pathArr.length; j++) {
                    if(j != 1) {
                        // 用 - 代替原本的 "/" (没有其他用户的权限)
                        fullName += "-";
                    }
                    fullName += pathArr[j];
                }
                fullName += ".git";
                String projectNameStr = pathArr[pathArr.length - 1];
                cmd = "cd " + projectNameStr + ".git";
                writeCmd(bw, cmd);
                // git push --mirror 把整个git仓库包括提交记录等提交到新的git仓库, 此处必须为 ssh 方式提交
                cmd = "git push --mirror git@gitlab.zhongwangkj.com:" + fullName;
                writeCmd(bw, cmd);
                cmd = "cd ../../";
                writeCmd(bw, cmd);
                bw.newLine();
            }
            // 执行完了暂停
            writeCmd(bw, "@pause");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            bw = null;
        }
        return R.success("success");
    }

    private void writeCmd(BufferedWriter bw, String cmd) throws IOException {
        bw.write(cmd);
        bw.newLine();
    }

}
