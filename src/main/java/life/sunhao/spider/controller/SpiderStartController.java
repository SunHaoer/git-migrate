package life.sunhao.spider.controller;

import io.swagger.annotations.Api;
import life.sunhao.spider.common.api.R;
import life.sunhao.spider.entity.GitUrl;
import life.sunhao.spider.service.IGitUrlService;
import life.sunhao.spider.spider.GitLabSpider;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: hsun
 * @Date: 2021/6/9 15:39
 * @Description:
 */
@Api(tags = "爬虫启动")
@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/spider/start")
public class SpiderStartController {

    final private GitLabSpider spider = new GitLabSpider();

    final private IGitUrlService gitUrlService;

    @GetMapping("/action")
    public R<Boolean> action() {
        // 需要获取的源 gitlab 仓库列表（暂未实现自动翻页）
        List<String> sourceUrls = new ArrayList<>();
        sourceUrls.add("https://git.zhongwangkj.com/admin/projects");
        sourceUrls.add("https://git.zhongwangkj.com/admin/projects?page=2&sort=latest_activity_desc");

        for(String url : sourceUrls) {
            List<GitUrl> gitUrls = spider.action(url, "sidebar_collapsed=false; _gitlab_session=d9ec7d7504082fafea2cfeed39715a3c; event_filter=all");
            boolean isSuccess = gitUrlService.saveBatch(gitUrls);
            if(!isSuccess) {
                log.info("保存失败");
            }
        }

        return R.success("success");
    }

}
