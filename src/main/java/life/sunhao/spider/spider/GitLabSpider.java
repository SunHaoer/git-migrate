package life.sunhao.spider.spider;

import life.sunhao.spider.common.enums.SourceEnums;
import life.sunhao.spider.common.exception.BizExceptionEnums;
import life.sunhao.spider.common.exception.BussinessException;
import life.sunhao.spider.entity.GitUrl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: hsun
 * @Date: 2021/6/9 15:51
 * @Description: 贝壳(ke)
 */
@Slf4j
//@Component
@AllArgsConstructor
public class GitLabSpider extends BaseSpider {

    // todo 源 git 账号
    final private String sourceGitUsername = "admin";

    // todo 源 git 密码
    final private String sourceGitPassword = "123456";

    // todo 源 git 域名
    final private String sourceGitDomain = "git.xxx.com";

    public List<GitUrl> action(String url, String cookie) {
        // 列表页
        Document document = super.document(url, SourceEnums.BEIKE.getHost(),  0, cookie);
        if(document == null) {
            throw new BussinessException(BizExceptionEnums.SPIDER_CONNECT_FAIL);
        }

        Elements elements = document.getElementsByClass("delete-project-button btn btn-danger");
        List<GitUrl> gitUrls = new ArrayList<>();
        for(Element element : elements) {
            String urlPostfix = element.attr("data-delete-project-url");
            log.info(urlPostfix);
            String sourceUrl = "http://" + sourceGitUsername + ":" + sourceGitPassword + "@" + sourceGitDomain + urlPostfix + ".git";
            gitUrls.add(new GitUrl(urlPostfix, sourceUrl));
        }

        return gitUrls;
    }

}
