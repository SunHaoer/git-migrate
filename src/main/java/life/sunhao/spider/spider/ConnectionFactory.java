package life.sunhao.spider.spider;

import life.sunhao.spider.common.enums.HeaderEnums;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

/**
 * @Auther: hsun
 * @Date: 2021/6/9 16:10
 * @Description:
 */
public class ConnectionFactory {

    public static Connection initConnection(String url, String host, String referer, String userAgent, String cookie) {
        Connection connection = Jsoup.connect(url);
        connection.header(HeaderEnums.COOKIE.getKey(), cookie);
        connection.header(HeaderEnums.HOST.getKey(), host);
        connection.header(HeaderEnums.ORIGIN.getKey(), referer);
        connection.header(HeaderEnums.REFERER.getKey(), referer);

        return connection;
    }

}
