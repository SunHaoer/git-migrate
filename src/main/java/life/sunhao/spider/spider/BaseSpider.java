package life.sunhao.spider.spider;

import life.sunhao.spider.common.enums.UserAgentEnums;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * @Auther: hsun
 * @Date: 2021/6/11 15:33
 * @Description:
 */
@Slf4j
public class BaseSpider {

    private static Integer USER_ARGENT_ARR_INDEX = 0;

    public Document document(String url, String host, Integer tryCnt, String cookie) {
        tryCnt++;
        Document document = null;
        try {
            log.info(url);
            try {
                Thread.sleep((long)(Math.random() * 1000L * 10));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            document = ConnectionFactory.initConnection(url, host, url, UserAgentEnums.userAgentArr[USER_ARGENT_ARR_INDEX], cookie).get();
        } catch (IOException e) {
            log.info("403 连接失败");
            USER_ARGENT_ARR_INDEX++;
            if(tryCnt.compareTo(UserAgentEnums.userAgentArr.length) == 0) {
                log.info("全部被封了，放弃了");
                return null;
            }
            if(USER_ARGENT_ARR_INDEX.compareTo(UserAgentEnums.userAgentArr.length) == 0) {
                USER_ARGENT_ARR_INDEX = 0;
            }
            return document(url, host, tryCnt, cookie);
        }
        return document;
    }

    public void sleep(Long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
