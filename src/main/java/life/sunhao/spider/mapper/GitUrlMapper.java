package life.sunhao.spider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import life.sunhao.spider.entity.GitUrl;

/**
 * @Auther: hsun
 * @Date: 2021/6/23 15:50
 * @Description:
 */
public interface GitUrlMapper extends BaseMapper<GitUrl> {
}
