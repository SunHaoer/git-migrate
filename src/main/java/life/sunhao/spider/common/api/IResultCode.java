package life.sunhao.spider.common.api;

import java.io.Serializable;

/**
 * @Auther: hsun
 * @Date: 2021/6/15 17:20
 * @Description:
 */
public interface IResultCode extends Serializable {
    String getMessage();

    int getCode();
}

