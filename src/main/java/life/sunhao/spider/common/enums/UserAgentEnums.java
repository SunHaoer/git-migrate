package life.sunhao.spider.common.enums;

import lombok.Getter;

/**
 * @Auther: hsun
 * @Date: 2021/6/9 15:53
 * @Description:
 */
@Getter
public enum UserAgentEnums {

    CHROME("chrome", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36"),
    POSTMAN("postman", "PostmanRuntime/7.28.0"),
    SAFARI_MAC("safari_mac", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50"),
    SAFARI_WIN("safari_win", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50"),
    IE9("IE9", "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0"),
    IE8("IE8", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0)"),
    IE7("IE7", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)"),
    IE6("IE6", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)"),
    FIREFOX_MAC("Firefox_mac", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0.1) Gecko/20100101 Firefox/4.0.1"),
    FIREFOX_WIN("Firefox_win", "Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1"),
    OPERA_MAC("Opera_mac", "Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; en) Presto/2.8.131 Version/11.11"),
    OPERA_WIN("Opera_win", "Opera/9.80 (Windows NT 6.1; U; en) Presto/2.8.131 Version/11.11"),
    AOYOU("aoyou", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Maxthon 2.0)"),
    TENCENT("tencent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; TencentTraveler 4.0)"),
    THE_WORLD("The World", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)"),
    SOUGOU("sougou", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; SE 2.X MetaSr 1.0; SE 2.X MetaSr 1.0; .NET CLR 2.0.50727; SE 2.X MetaSr 1.0)"),
    AVANT("Avant", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Avant Browser)"),
    GREEN("Green", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)"),
    EDGE("edge", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36 Edg/91.0.864.41");

    private String key;

    private String value;

    static public String[] userAgentArr;

    UserAgentEnums(String key, String value) {
        this.key = key;
        this.value = value;
    }

    static {
        UserAgentEnums[] values = UserAgentEnums.values();
        userAgentArr = new String[values.length];
        for(int i = 0; i < values.length; i++) {
            userAgentArr[i] = values[i].getValue();
        }
    }

}
